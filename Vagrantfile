# Vagrantfile

#commands 
#vagrant up
#vagrant halt
#vagrant reload --provision
#vagrant reload backend1 --provision (to rebuild one vm)
#vagrant destroy

# Define the base box for the VMs (Ubuntu 20.04 LTS)
Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/focal64"
  config.vm.boot_timeout = 600

  #starting_ip = "192.168.1.15"
  #vms
  (1..3).each do |i|
    #current_ip = IPAddr.new(starting_ip).to_s[0..-2] + (i + 14).to_s

    config.vm.define "backend#{i}" do |backend|
      backend.vm.network "public_network", type: "static", ip: current_ip.to_s
      backend.vm.provider "virtualbox" do |vb|
        vb.name = "backend#{i}"
        vb.memory = 1024
      end
      backend.vm.provision "shell", inline: <<-SCRIPT
        sudo apt-get update
        sudo apt-get install -y vim
        sudo apt-get install -y prometheus-node-exporter
      SCRIPT
    end
  end

  #sudo ip route del default via 192.168.1.254 dev enp0s8 proto dhcp src 192.168.1.21 metric 100

  #grafana
  config.vm.define "grafana" do |grafana|
    grafana.vm.network "public_network", type: "static", ip: "192.168.1.118"
    grafana.vm.provider "virtualbox" do |vb|
      vb.name = "grafana"
      vb.memory = 1024
    end

    grafana.vm.provision "shell", inline: <<-SCRIPT
      sudo apt-get install -y apt-transport-https software-properties-common wget
      sudo mkdir -p /etc/apt/keyrings/
      wget -q -O - https://apt.grafana.com/gpg.key | gpg --dearmor | sudo tee /etc/apt/keyrings/grafana.gpg > /dev/null
      echo "deb [signed-by=/etc/apt/keyrings/grafana.gpg] https://apt.grafana.com stable main" | sudo tee -a /etc/apt/sources.list.d/grafana.list
      sudo add-apt-repository "deb https://packages.grafana.com/oss/deb stable main"
      sudo apt-get update
      sudo apt-get install -y grafana
      sudo apt-get install -y prometheus-node-exporter
    SCRIPT
  end

  # Prometheus server
  config.vm.define "prometheus" do |prometheus|
    prometheus.vm.network "public_network", type: "static", ip: "192.168.1.119"
    prometheus.vm.provider "virtualbox" do |vb|
      vb.name = "prometheus"
      vb.memory = 1024
    end
  
    # SSH Configuration
    # prometheus.ssh.host = "127.0.0.1"
    # prometheus.ssh.port = 2203
    # prometheus.ssh.username = "vagrant"
    # prometheus.ssh.private_key_path = "./.vagrant/machines/prometheus/virtualbox/private_key"

    prometheus.vm.provision "shell", inline: <<-SCRIPT
      sudo apt-get update
      sudo apt-get install -y prometheus
      cp /home/jean-louis/infra-training-vagrant/prometheus.yml /etc/prometheus/
    SCRIPT
  end

  #load balancer
  config.vm.define "loadbalancer" do |lb|
    lb.vm.network "public_network", type: "static", ip: "192.168.1.120"
    lb.vm.provider "virtualbox" do |vb|
      vb.name = "loadbalancer"
      vb.memory = 4096
    end

    # SSH Configuration
    # lb.ssh.host = "127.0.0.1"
    # lb.ssh.port = 2204
    # lb.ssh.username = "vagrant"
    # lb.ssh.private_key_path = "./.vagrant/machines/loadbalancer/virtualbox/private_key"

    #script for installing HAProxy
    lb.vm.provision "shell", inline: <<-SCRIPT
      sudo apt-get update
      sudo apt-get install -y haproxy
      sudo apt-get install -y vim
      sudo apt-get install -y prometheus-node-exporter
    SCRIPT

    #Copy HAProxy configuration file to the VM
    lb.vm.provision "shell" do |s|
      s.inline = "cp /home/jean-louis/infra-training-vagrant/haproxy.cfg /etc/haproxy/haproxy.cfg"
    end

    # #script for editing dynamically haproxy.cfg
    # lb.vm.provision "shell", inline: <<-SCRIPT     
    #   BACKEND_IPS=$(vagrant status --machine-readable | awk -F, '/backend[0-9]/{print $6}')
    #   BACKEND_SERVERS=""
    #   for IP in $BACKEND_IPS; do
    #     BACKEND_SERVERS="$BACKEND_SERVERS\n    server backend${IP} ${IP}:80 check"
    #   done
  
    #   sudo sed -i '/backend backend/{N;s/}\n/}\n'"$BACKEND_SERVERS"'\n/}' /etc/haproxy/haproxy.cfg
  
    #   sudo systemctl restart haproxy
    # SCRIPT

    # Prometheus dynamic configuration
    config.vm.define "prometheus" do |prometheus|
      prometheus.vm.provision "shell", inline: <<-SCRIPT
        BACKEND_IPS=$(vagrant status --machine-readable | awk -F, '/backend[0-9]/{print $6}')
        BACKEND_TARGETS=""
        for IP in $BACKEND_IPS; do
          BACKEND_TARGETS="$BACKEND_TARGETS,${IP}:9100"
        done
        BACKEND_TARGETS=${BACKEND_TARGETS:1}  # Remove leading comma

        echo "scrape_configs:" | sudo tee -a /etc/prometheus/prometheus.yml
        echo "  - job_name: 'node-exporter'" | sudo tee -a /etc/prometheus/prometheus.yml
        echo "    static_configs:" | sudo tee -a /etc/prometheus/prometheus.yml
        echo "      - targets: ['$BACKEND_TARGETS']" | sudo tee -a /etc/prometheus/prometheus.yml

        sudo systemctl restart prometheus
      SCRIPT
    end
  end

  #Ansible server
  # config.vm.define "ansible" do |ansible|
  #   config.vm.network "public_network", type: "static", ip: "192.168.1.121"
  #   ansible.vm.provider "virtualbox" do |vb|
  #     vb.name = "ansible"
  #     vb.memory = 4096
  #   end
  # end
  #   ansible.vm.provision "shell", inline: <<-SCRIPT
  #     sudo apt-get update
  #     sudo apt-get install -y ansible
  #     sudo apt-get install -y prometheus-node-exporter
  #   SCRIPT
  # end
end
